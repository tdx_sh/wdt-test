#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <systemd/sd-daemon.h>
#include <systemd/sd-journal.h>
#include <fcntl.h>
#include <time.h>
#include <stdlib.h>


int main(int argc, char **argv)
{
	int interval;
	char *env;
	int i = 0;
    sd_notify (0, "READY=1");
    
    env = getenv("WATCHDOG_USEC");
	if(env != NULL)
	{
		interval = atoi(env)/(2*1000000);
		sd_journal_print(LOG_INFO, "wdt reset interval: %d s", interval);

	}
	else
	{
		sd_journal_print(LOG_INFO, "Failed to fetch WATCHDOG_USEC");
		return -1;
	}

	sd_notify (0, "WATCHDOG=1");
    
    while(1)
    {
        if(i != 3)
			sleep(interval);
		else
		{
			sd_journal_print(LOG_INFO, "Extented time to sleep");
			sleep(interval*4);
		}
        
        sd_notify (0, "WATCHDOG=1");
        i++;
    }
    
    return 0;
}


